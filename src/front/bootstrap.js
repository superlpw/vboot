/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月02日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
import 'bootstrap/dist/css/bootstrap.min.css';
import 'admin-lte/dist/css/AdminLTE.min.css';
import 'admin-lte/dist/css/skins/skin-purple-light.css';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrapvalidator/dist/css/bootstrapValidator.min.css';
import 'pretty-checkbox/src/pretty-checkbox.scss';
import 'vue-snotify/styles/simple.css';
import 'vx-easyui/dist/themes/gray/easyui.css';
import 'vx-easyui/dist/themes/icon.css';
import 'vx-easyui/dist/themes/vue.css';
import './styles/app.scss';

import 'jquery';
import 'bootstrap';
import 'admin-lte';
import 'jquery-slimscroll';
import 'bootstrapvalidator';

try {
  $('body').addClass('sidebar-mini skin-purple-light fixed');
  Object.assign($.fn.bootstrapValidator.DEFAULT_OPTIONS, {
    feedbackIcons: {
      validating: 'fa fa-refresh'
    },
    trigger: 'blur'
  });
  Object.assign($.fn.bootstrapValidator.validators, {
    mobile: {
      validate: (validator, $field, options) => {
        var value = $field.val();
        if (value && !(/^1(3|4|5|7|8)\d{9}$/.test(value))) {
          return {
            valid: false,
            message: '手机号码有误，请重填'
          };
        }
        return true;
      }
    },
    idcard: {
      validate: (validator, $field, options) => {
        var value = $field.val();
        if (value && !(/(^\d{15}$)|(^\d{17}([0-9]|X)$)/.test(value))) {
          return {
            valid: false,
            message: '身份证号码有误，请重填'
          };
        }
        return true;
      }
    }
  });
} catch (e) {
}
