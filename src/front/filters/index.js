/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月02日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */

import moment from 'moment';

export default (Vue) => {
  Vue.filter('dateFormat', function(value, format = 'YYYY-MM-DD hh:mm:ss') {
    return moment(value).format(format);
  });
};
