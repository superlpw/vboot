package cn.gson.vboot.controller.system;

import cn.gson.vboot.common.AcLog;
import cn.gson.vboot.common.JsonResult;
import cn.gson.vboot.model.pojo.Permission;
import cn.gson.vboot.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description :  权限资源管理</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月07日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@RequestMapping("/system/permission")
public class PermissionController {

    @Autowired
    PermissionService permissionService;

    @GetMapping("/list")
    @AcLog(module = "权限", action = "列表")
    public JsonResult list() {
        return JsonResult.success(permissionService.list());
    }

    @GetMapping("/get")
    @AcLog(module = "权限", action = "获取", params = "id")
    public JsonResult get(Long id) {
        return JsonResult.success(permissionService.getById(id));
    }

    @PostMapping("/updateEnable")
    @AcLog(module = "权限", action = "更新状态", params = {"id", "enable"})
    public JsonResult updateEnable(Long id, Boolean enable) {
        permissionService.updateEnable(id, enable);
        return JsonResult.success();
    }

    @PostMapping("/save")
    @AcLog(module = "权限", action = "创建")
    public JsonResult save(@Valid Permission permission) {
        permission.setId(null);
        permissionService.saveOrUpdate(permission);
        return JsonResult.success();
    }

    /**
     * 更新权限信息
     *
     * @param id         定义出来，表示 更新是 id 一定需要给定值
     * @param permission
     * @return
     */
    @PostMapping("/update")
    @AcLog(module = "权限", action = "更新", params = "id")
    public JsonResult update(@RequestParam long id, @Valid Permission permission) {
        permissionService.saveOrUpdate(permission);
        return JsonResult.success();
    }

    @GetMapping("/delete")
    @AcLog(module = "权限", action = "删除", params = "id")
    public JsonResult delete(Long id) {
        permissionService.deleteById(id);
        return JsonResult.success();
    }
}
